import {Button, Container, ListGroup, Navbar} from 'react-bootstrap'
import File from './File'
import React, {useEffect, useRef, useState} from 'react'
import axios from 'axios'

function Admin(props) {
  const [file, setFile] = useState([])
  const fileRef = useRef()

  const add = (info) => {
    console.log(info)
    const data = new FormData()
    data.append('file', info.target.files[0])
    const item = axios.post(`${props.api}/upload`, data)
    item.then(q => {
      alert('Успешно загружено')
      setFile(p => [q.data, ...p])
    }).catch(w => {
      console.log(w)
    })
  }

  useEffect(() => {
    axios.get(`${props.api}/document`).then(q => {
      setFile(q.data)
    })
  }, [])

  return (
    <>
      <Navbar bg="dark" expand="lg" style={{height: '9vh'}}>
        <Container fluid className={'d-flex flex-nowrap justify-content-between'}>
          <Navbar.Brand style={{
            color: 'white',
            marginLeft: '3vw',
            flexShrink: 1,
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            whiteSpace: 'nowrap',
          }}>Личный кабинет</Navbar.Brand>
          <div className="d-flex file ">
            <input type={'file'}
                   ref={fileRef}
                   onChange={(info) => add(info)}
                   accept={'.pdf, .jpg, .png, .jpeg, .txt, .doc, '}
                   style={{display: 'none'}}/>
            <Button variant="success" onClick={() => fileRef.current.click()} className={'btn_1'}
                    style={{width: '100%'}}>Добавить файл</Button>
          </div>
        </Container>
      </Navbar>
      <div>
        <Container className={'my-5 list_file'}>
          <ListGroup style={{width: '100%'}}>
            {file.map((e, index) => {
              return (
                <File name={e} id={e.id} file={setFile} api={props.api}/>
              )
            })}
          </ListGroup>
        </Container>
      </div>
    </>
  )
}

export default Admin
