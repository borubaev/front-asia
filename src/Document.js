import React, {useEffect, useState} from 'react'
import {useParams} from 'react-router'
import axios from 'axios'

function Document(props) {
  const {id} = useParams()
  const [doc, setDoc] = useState(null)
  let z = ''

  useEffect(() => {
    axios.get(`${props.api}/document`, {
      params: {id},
    }).then(q => {
      setDoc(q.data)
    }).catch(w => {
      alert('Error')
    })
  }, [id])
  if (!doc) {
    return <h1>Подождите...</h1>
  } else {
    z = doc.name.split('.')
  }

  return (
    <>
      {z[z.length - 1] === 'pdf' ?
        <div style={{width: '100vw', height: '100vh'}}>
          <iframe src={`https://docs.google.com/gview?url=${props.api}${doc.path}&embedded=true`} width="100%" height="100%"/>
        </div> :
        z[z.length - 1] === 'jpg' || z[z.length - 1] === 'jpeg' || z[z.length - 1] === 'png' ?
          <div style={{width: '100vw', height: '100vh', backgroundColor: 'lightgrey'}}>
            <div className={'d-flex justify-content-center align-items-center txt_file'}>
              <img src={`https://api.asia-toefl-test.org${doc.path}`} style={{width: '90%'}}/>
            </div>
          </div> :
          <div style={{width: '100vw', height: '100vh', backgroundColor: 'lightgrey'}}>
            <div className={'txt_file py-4'} style={{fontSize: '50px'}}>
              <iframe src={`https://api.asia-toefl-test.org${doc.path}`} style={{width: '100%', height: '100%'}}/>
            </div>
          </div>
      }
    </>
  )
}

export default Document
