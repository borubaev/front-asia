import React, {useState} from "react";
import {ListGroupItem, Button} from "react-bootstrap";
import axios from "axios";
import QRCode from "react-qr-code";
import Modal from "react-bootstrap/Modal";

function File(props) {
    const id = props.id
    const [show, setShow] = useState(false)

    const delet = (id) => {
        const ques = window.confirm('Вы действительно хотите удалить этот файл?')
        if (ques) {
            axios.delete(`${props.api}/delete`, {
                params: {id}
            }).then(() => {
                alert('Успешно удалено')
                props.file(prev => {
                    return prev.filter(q => q.id !== id)

                })
            })
        }
    }
    const qr_code = () => {
        setShow(true)
    }
    const close = () => {
        setShow(false)
    }

    function downloadQRCode() {
        const svg = document.getElementById("qr-gen");
        const svgData = new XMLSerializer().serializeToString(svg);
        const canvas = document.createElement("canvas");
        const ctx = canvas.getContext("2d");
        const img = new Image();
        img.onload = () => {
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img, 0, 0);
            const pngFile = canvas.toDataURL("image/png");
            const downloadLink = document.createElement("a");
            downloadLink.download = "QRCode";
            downloadLink.href = `${pngFile}`;
            downloadLink.click();
        };
        img.src = `data:image/svg+xml;base64,${btoa(svgData)}`;
        setShow(false)
    }


    return (
        <>
            <ListGroupItem className={'xl:flex-1 d-sm-flex align-items-center justify-content-between'}>
                <div className={'d-flex align-items-center list_file'}>
                    <div className={'fa fa-file-pdf-o fa-3x'}/>
                    <h4 style={{margin: '15px', color: 'black'}}>{props.name.name}</h4>
                </div>
                <Modal show={show} onHide={close} className={'my-5'}>
                    <Modal.Header closeButton/>
                    <Modal.Body align={'center'}>
                        <QRCode
                            size={256}
                            style={{height: "auto", maxWidth: "70%", width: "100%"}}
                            value={'https://asia-toefl-test.org/doc/' + id}
                            viewBox={`0 0 256 256`}
                            id={'qr-gen'}
                        />
                        <Modal.Footer className={'my-2'} align={'center'}>
                            <div>
                                <Button onClick={() => downloadQRCode()}>Скачать Qr-Code</Button>
                            </div>
                        </Modal.Footer>
                    </Modal.Body>

                </Modal>
                <div>
                    <Button style={{margin: '10px'}} variant={'outline-dark'} onClick={() => delet(id)}>Удалить</Button>
                    <Button style={{margin: '10px'}} variant={'outline-dark'}
                            onClick={() => qr_code()}>Сгенерировать</Button>
                </div>
            </ListGroupItem>
        </>
    )
}

export default File
