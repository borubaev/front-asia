import './App.css'
import React, {useEffect, useState} from 'react'
import Admin from './Admin'
import Auth from './Auth'

function App() {
  const API_URL = 'http://207.154.217.12:5000'

  const [user, setUser] = useState(JSON.parse(localStorage.getItem('user')) || null)

  useEffect(() => {
    localStorage.setItem('user', JSON.stringify(user))
  }, [user])

  if (!user) {
    return <Auth setUser={setUser} api={API_URL}/>
    // return (
    //   <BrowserRouter>
    //     <Routes>
    //       <Route path={'/'} element={<Auth setUser={setUser} api={API_URL}/>}/>
    //       <Route path={'/doc/:id'} element={<Document api={API_URL}/>}/>
    //     </Routes>
    //   </BrowserRouter>
    // )
  }

  return (
    <>
      <Admin api={API_URL}/>}/>
      {/*<BrowserRouter>*/}
      {/*  <Routes>*/}
      {/*    <Route path={'/'} element={<Admin api={API_URL}/>}/>*/}
      {/*    <Route path={'/doc/:id'} element={<Document api={API_URL}/>}/>*/}
      {/*  </Routes>*/}
      {/*</BrowserRouter>*/}
    </>
  )
}

export default App
