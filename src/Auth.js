import {Button} from 'react-bootstrap'
import {useState} from 'react'
import axios from 'axios'

function Auth(props) {
  const [login, setLogin] = useState('')
  const [password, setPassword] = useState('')

  const add = () => {
    axios.post(`${props.api}/login`, {
      login: login,
      password: password,
    }).then(q => {
      if (q.request.status) {
        props.setUser({login, password})
      }
    })
    setLogin('')
    setPassword('')
  }

  return (
    <>
      <div className={'auth d-flex justify-content-center flex-column align-items-center'}
           style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)'}}>
        <h3 align={'center'}>Введите логин и пароль...</h3>
        <div className={'d-flex flex-column justify-content-center'} style={{width: '100%'}}>
          <input className={'inp'} type={'text'} onChange={(event) => setLogin(event.target.value)} value={login}/>
          <input className={'inp'} type={'password'} onChange={(event) => setPassword(event.target.value)}
                 value={password}/>
        </div>
        <Button variant={'outline-secondary'} style={{width: '70%', marginTop: '3vh'}}
                onClick={add}
        >Войти</Button>
      </div>
    </>
  )
}

export default Auth
